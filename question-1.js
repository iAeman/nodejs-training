const sum = (...theArgs) => {
    if (theArgs.length < 2) {
        console.log("Error: Cannot sum these values!")
        return 0;
    }
    return theArgs.reduce((previous, current) => {
      return previous + current;
    });
  }
  
console.log(sum(1,2));
console.log(sum(1,2));
console.log(sum(1,2,3));
console.log(sum(1,2,3,4));
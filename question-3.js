const sum = (value) => {
    if (value) {
      return (arg) => {
        if (arg) {
          return sum(value + arg);
        } else {
          return value;
        }
      };
    }
    return 0;
  };


  console.log(sum(1)(2)());
  console.log(sum(1)(2)(3)());
  console.log(sum(1)(2)(3)(4)());
  const results = {};

  function serverCall(url, myCallback) {
    const argsKey = JSON.stringify(url);
      if (results[argsKey]) {
          console.log('already visited this page!')
          myCallback(results[argsKey]);
          return;
      }
    const https = require('https')
    const options = {
      hostname: url,
      method: 'GET'
    }
    
    const req = https.request(options, res => {
        const statusCode = JSON.stringify(res.statusCode);
        results[argsKey] = statusCode;
        myCallback(statusCode);
    })

    req.on('error', error => {
        results[argsKey] = error;

    })
    req.end()
  }

  function myCallback(param) {
      console.log(param);
  }

  serverCall('google.com', myCallback);


  setTimeout(() => {  serverCall('google.com', myCallback); }, 2000);

  setTimeout(() => {  serverCall('google.com', myCallback); }, 2000);

  
  

const populate = function(callback) {
    for (let i = 0; i < this.length ; i++) {
        if (this[i].includes('.com')) {
            this[i] = 'http://www.' + this[i];
        } else {
            throw 'Array does not contain valid urls!';
        }
    }
}

Array.prototype.populate = populate;

let myArray = ['google.com', 'facebook.com'];
try {
    myArray.populate();
    console.log(myArray);
} catch (error) {
    console.log(error);
}